using UnityEngine;
using UnityEngine.UI;

public class EndGameScript : MonoBehaviour
{

    private Text resultatsText;
    private string missatgeFinal;

    void Start()
    {
        resultatsText = GameObject.Find("Resultats").GetComponent<Text>();

        if (EndGameData.playerTotalWins > EndGameData.computerTotalWins)
        {
            missatgeFinal = "<size=20>Enhorabona! Has guanyat el joc!</size>";
        }
        else
        {
            missatgeFinal = "<size=20>Vaja, has perdut! Segueix intentant!</size>";
        }

        resultatsText.text = "Rondes totals: " + EndGameData.totalRounds + 
            "\nVict�ries del jugador: " + EndGameData.playerTotalWins + 
            "\nVict�ries de l'ordinador: " + EndGameData.computerTotalWins + 
            "\nTemps emprat: " + EndGameData.totalTime + 
            "\n\n" + missatgeFinal;
    }
}
