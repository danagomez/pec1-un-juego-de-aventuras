using System;

public class DialogueNode
{
    public string History;
    public string[] Answers;
    public bool IsFinal;
    public DialogueNode[] NextNode;
    public Action OnNodeVisited;
}
