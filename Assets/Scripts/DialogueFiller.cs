using UnityEngine;
using UnityEngine.UI;

public class DialogueFiller
{
    [System.Serializable]
    public class Insult
    {
        public string computer;
        public string player;
    }

    [System.Serializable]
    public class Insults
    {
        public Insult[] insults;
    }

    private static Insults totalInsults;
    private static int numInsults;

    private static int insultIndex = -1;
    private static int answerIndex = -1;
    
    private static GameObject victoriesJugador;
    private static GameObject victoriesOrdinador;
    public static int playerTotalWins;
    public static int computerTotalWins;
    private static Image winMark;

    private static Animator playerAnimator;
    private static Animator computerAnimator;

    private const int PLAYER = 0;
    private const int COMPUTER = 1;

    private static AudioSource attackAudio;
    private static AudioSource deadAudio;
    private static AudioSource winAudio;

    public static DialogueNode FillStory()
    {
        victoriesJugador = GameObject.Find("VictoriesJugador");
        victoriesOrdinador = GameObject.Find("VictoriesOrdinador");
        winMark = Resources.Load<Image>("Prefabs/MarcaWin");

        playerAnimator = GameObject.Find("Player").GetComponent<Animator>();
        computerAnimator = GameObject.Find("Computer").GetComponent<Animator>();

        attackAudio = GameObject.Find("AttackAudio").GetComponent<AudioSource>();
        deadAudio = GameObject.Find("DeadAudio").GetComponent<AudioSource>();
        winAudio = GameObject.Find("WinAudio").GetComponent<AudioSource>();

        playerTotalWins = 0;
        computerTotalWins = 0;

        totalInsults = JsonUtility.FromJson<Insults>(Resources.Load<TextAsset>("Text/insults").text);
        numInsults = totalInsults.insults.Length;

        var root = CreateNode(
            "Comen�a la lluita de insults!",
            new[] {
            "Seg�ent"});

        // Aquest node serveix per mostrar els insults disponibles del jugador
        var insultsJugador = CreateNode(
            "",
            new string[numInsults]);
        insultsJugador.OnNodeVisited = () =>
        {
            playerAnimator.SetTrigger("idle");
            computerAnimator.SetTrigger("idle");
        };

        // Tant el jugador com l'ordinador tenen un array de nodes amb cadascun dels insults/respostes disponibles
        DialogueNode[] playerInsults = new DialogueNode[numInsults];
        DialogueNode[] playerAnswers = new DialogueNode[numInsults];

        DialogueNode[] computerInsults = new DialogueNode[numInsults];
        DialogueNode[] computerAnswers = new DialogueNode[numInsults];

        var playerWins = CreateNode(
            "Has guanyat!",
            new[] {
                ""});
        playerWins.IsFinal = true;
        playerWins.OnNodeVisited = () =>
        {
            CanviarColor(COMPUTER);
        };

        var computerWins = CreateNode(
            "Has perdut!",
            new[] {
                ""});
        computerWins.IsFinal = true;
        computerWins.OnNodeVisited = () =>
        {
            CanviarColor(COMPUTER);
        };

        // Amb aquest bucle, omplim els nodes d'insults i respostes tant del jugador com del ordinador
        for (int i = 0; i < numInsults; i++)
        {
            Insult insult = totalInsults.insults[i];

            // playerInsults
            playerInsults[i] = CreateNode(
                insult.computer,
                new string[] { "Seg�ent" });

            // insultsJugador
            insultsJugador.Answers[i] = insult.computer;
            insultsJugador.NextNode[i] = playerInsults[i];

            // playerAnswers
            playerAnswers[i] = CreateNode(
                insult.player,
                new[] { "Seg�ent" });

            // computerInsults
            computerInsults[i] = CreateNode(
                insult.computer,
                new string[numInsults]);

            // computerAnswers
            computerAnswers[i] = CreateNode(
                insult.player,
                new[] { "Seg�ent" });
        }

        // Amb aquest bucle, definim les accions del OnNodeVisited de cada node
        for (int i = 0; i < numInsults; i++)
        {
            int index = i;

            // playerInsults
            playerInsults[i].OnNodeVisited = () =>
            {
                CanviarColor(PLAYER);
                int respostaComputer = 0;
                // L'ordinador pot guanyar el (50% o el 1/16) de vegades
                if ((Random.Range(0, 2)) == 0) {
                    respostaComputer = index;
                }
                else {
                    respostaComputer = Random.Range(0, numInsults);
                }
                
                playerInsults[index].NextNode[0] = computerAnswers[respostaComputer];
                insultIndex = index;
            };

            // playerAnswers
            playerAnswers[i].OnNodeVisited = () =>
            {
                CanviarColor(PLAYER);
                answerIndex = index;
                resolveRound(false, playerAnswers[index], insultsJugador, computerInsults, playerWins, computerWins);
            };

            // computerInsults
            computerInsults[i].OnNodeVisited = () =>
            {
                playerAnimator.SetTrigger("idle");
                computerAnimator.SetTrigger("idle");
                CanviarColor(COMPUTER);
                insultIndex = index;
            };
            for (int j = 0; j < numInsults; j++)
            {
                computerInsults[i].Answers[j] = totalInsults.insults[j].player;
                computerInsults[i].NextNode[j] = playerAnswers[j];
            }

            // computerAnswers
            computerAnswers[i].OnNodeVisited = () =>
            {
                CanviarColor(COMPUTER);
                answerIndex = index;
                GameManager.WriteDialog(totalInsults.insults[answerIndex].player);
                resolveRound(true, computerAnswers[index], insultsJugador, computerInsults, playerWins, computerWins);
            };
        }

        root.NextNode[0] = NovaRonda(-1, insultsJugador, computerInsults);

        return root;
    }

    private static DialogueNode CreateNode(string history, string[] options)
    {
        var node = new DialogueNode
        {
            History = history,
            Answers = options,
            NextNode = new DialogueNode[options.Length],
            IsFinal = false
        };
        return node;
    }

    private static DialogueNode NovaRonda(int jugadorHaGuanyat, DialogueNode insultsJugador, DialogueNode[] computerInsults)
    {
        /* Aquest metode comprova qui ha guanyat la ronda anterior, i qui hagi guanyat comen�a aquesta ronda. 
         * Si es la primera ronda, hi ha un 50% de probabilitat de que comen�i el jugador. 
         */
        DialogueNode newRound;
        if (jugadorHaGuanyat < 0)
        {
            if (Random.Range(0, 2) == 0){
                newRound = insultsJugador;
                Debug.Log("Player starts");
            }
            else{
                newRound = computerInsults[Random.Range(0, numInsults)]; // L'ordinador escull un insult aleatori
            }
        }
        else
            if (jugadorHaGuanyat == 0){
                newRound = computerInsults[Random.Range(0, numInsults)];
            }
            else{
                newRound = insultsJugador;
            }

        return newRound;
    }

    private static void resolveRound(bool player_started_round, DialogueNode answered, DialogueNode insultsJugador, DialogueNode[] computerInsults, DialogueNode playerWins, DialogueNode computerWins)
    {
        /* Aquest metode comprova si el jugador ha guanyat la ronda actual (si ha donat la resposta correcta). 
         * Si el jugador ha guanyat, l'ordinador perd una vida i comen�a una nova ronda si encara t� vides. 
         * El mateix passa si el jugador perd, per� perd una vida aquest. 
         */
        bool player_won_round = player_started_round ^ (insultIndex == answerIndex);
        EndGameData.totalRounds++;
        if (player_won_round)
        {
            playerTotalWins++;
            if (playerTotalWins == 3){
                answered.NextNode[0] = playerWins;
            }
            else{
                answered.NextNode[0] = NovaRonda(1, insultsJugador, computerInsults);
            }
            MonoBehaviour.Instantiate(winMark, victoriesJugador.transform, true);
            winAudio.Play(0);
            EndGameData.playerTotalWins = playerTotalWins;
        }
        else
        {
            computerTotalWins++;
            if (computerTotalWins == 3){
                answered.NextNode[0] = computerWins;
            }
            else{
                answered.NextNode[0] = NovaRonda(0, insultsJugador, computerInsults);
            }
            MonoBehaviour.Instantiate(winMark, victoriesOrdinador.transform, true);
            winAudio.Play(0);
            EndGameData.computerTotalWins = computerTotalWins;
        }

        if (computerTotalWins == 3 || playerTotalWins == 3)
        {
            playerAnimator.SetBool("gameOver", true);
            playerAnimator.SetTrigger("die");
            computerAnimator.SetBool("gameOver", true);
            computerAnimator.SetTrigger("die");
            deadAudio.Play(0);
        }
        attackAudio.Play(0);
        playerAnimator.SetBool("wonRound", player_won_round);
        computerAnimator.SetBool("wonRound", !player_won_round);
        playerAnimator.SetTrigger("hurt");
        playerAnimator.SetTrigger("attack");
        computerAnimator.SetTrigger("hurt");
        computerAnimator.SetTrigger("attack");
    }

    private static void CanviarColor(int newSpeaker)
    {
        GameManager.CanviarColor(newSpeaker);
    }
}
