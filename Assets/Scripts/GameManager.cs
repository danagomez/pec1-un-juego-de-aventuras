using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    private static Text insultText;
    private GameObject optionsParent;
    private GameObject optionPrefab;

    private DialogueNode currentNode;

    private static AudioSource textAudio;
    private static AudioSource pauseAudio;
    public static AudioSource buttonAudio;

    private GameObject pauseScreen;
    private bool paused = false;

    private const int PLAYER = 0;
    private const int COMPUTER = 1;

    private void Start()
    {
        EndGameData.totalRounds = 0;
        instance = this;
        pauseScreen = GameObject.Find("PantallaPausa");
        pauseScreen.SetActive(false);

        insultText = GameObject.Find("TextInsult").GetComponent<Text>();
        optionsParent = GameObject.Find("Contingut");
        optionPrefab = Resources.Load<GameObject>("Prefabs/Opcio");

        textAudio = GameObject.Find("TextAudio").GetComponent<AudioSource>();
        pauseAudio = GameObject.Find("PauseAudio").GetComponent<AudioSource>();
        buttonAudio = GameObject.Find("ButtonAudio").GetComponent<AudioSource>();

        insultText.text = string.Empty;
        currentNode = DialogueFiller.FillStory();

        FillUI();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            pauseScreen.SetActive(!pauseScreen.activeSelf);
            paused = !paused;
            pauseAudio.Play(0);
        } 
    }

    void FillUI()
    {
        // Primer escriu la frase actual en la part superior, i un cop s'ha acabat d'escriure mostra les opcions.
        DialogueNode node = currentNode;
        

        foreach (Transform child in optionsParent.transform)
        {
            Destroy(child.gameObject);
        }

        WriteSentence(node.History);
    }

    public static void WriteDialog(string text)
    {
        instance.WriteSentence(text);
    }

    private void WriteSentence(string text)
    {
        StopAllCoroutines();
        StartCoroutine(TypeSentence(text));
    }

    IEnumerator TypeSentence(string text)
    {
        // Escriu la frase poc a poc, donant un efecte de typewriter.

        insultText.text = string.Empty;
        foreach (char letter in text.ToCharArray())
        {
            insultText.text += letter;
            textAudio.Play(0);
            yield return new WaitForSeconds(0.03f);
        }

        FillOptions();
    }

    private void FillOptions()
    {
        int i = 0;
        foreach (string answer in currentNode.Answers)
        {
            GameObject newOption = Instantiate(optionPrefab, optionsParent.transform, true);
            newOption.GetComponent<Text>().text = answer;

            ButtonListener(newOption.GetComponent<Button>(), i, currentNode.IsFinal);

            i++;
        }
    }

    private void ButtonListener(Button button, int index, bool isFinal)
    {
        if (!isFinal)
            button.onClick.AddListener(() => { SelectAnswer(index); });
        else
        {
            StartCoroutine(EndGame());
        }
    }

    IEnumerator EndGame()
    {
        Debug.Log("Fi del joc");
        yield return new WaitForSeconds(4);

        var ts = TimeSpan.FromSeconds(Time.timeSinceLevelLoad);
        EndGameData.totalTime = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);

        SceneChanger.GoToEndScreen();
    }

    private void SelectAnswer(int answerIndex)
    {
        if (paused)
            return;
        
        currentNode = currentNode.NextNode[answerIndex];

        buttonAudio.Play(0);

        if (currentNode.OnNodeVisited != null)
            currentNode.OnNodeVisited.Invoke();

        FillUI();
        
    }

    public static void CanviarColor(int speaker)
    {
        if (speaker == PLAYER)
            insultText.color = new Color32(255, 172, 162, 255);
        else
            insultText.color = new Color32(191, 255, 255, 255);
    }
}
