using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    private static AudioSource buttonAudio;

    private void Start()
    {
        buttonAudio = GameObject.Find("ButtonAudio").GetComponent<AudioSource>();
    }

    public void ReturnToMainMenu()
    {
        buttonAudio.Play(0);
        SceneManager.LoadScene("MenuInici");
    }

    public void StartNewGame()
    {
        buttonAudio.Play(0);
        SceneManager.LoadScene("Joc");
    }

    public void ExitGame()
    {
        buttonAudio.Play(0);
        Debug.Log("Sortint del joc...");
        Application.Quit();
    }

    public static void GoToEndScreen()
    {
        buttonAudio.Play(0);
        SceneManager.LoadScene("PantallaFinal");
    }
}
