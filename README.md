# PEC 1 - Un Juego de Aventuras

Aquest joc està basat en el duel d'insults del joc Monkey Island. Ha sigut creat per a un projecte del curs de Programació de Videojocs 2D de la UOC.

Clica [aquí](https://youtu.be/AVgiA6p4h2Y) per un vídeo de demostració del joc i del 'Device Simulator' de Unity.

Clica [aquí](https://danagomezbalada.itch.io/pec-1-monkey-island) per jugar a la versió de WebGL online:

## Instal·lació

Per tal d'instal·lar aquest joc, hem de descarregar una de les carpetes dins de "[Builds](https://gitlab.com/danagomez/pec1-un-juego-de-aventuras/-/tree/master/Builds)". Podem escollir entre **WebGL, Android i Windows**.


## Funcionalitats del joc

Un cop executem el joc, veurem la pantalla inicial. En aquesta pantalla escoltarem música de fons i veurem dos botons: 
- **Començar joc**: Ens portarà a la *scene* principal del joc.
- **Sortir del joc**: Es tancarà el joc.

També veurem a la part dreta inferior un text informatiu.

Un cop comença el joc principal, veurem una caixa de text a la part inferior, dos personatges i un text que ens incitarà a començar el duel d'insults. També escoltarem una nova música de fons, més apropiada per un duel. Un cop fem clic a "Següent" comença la primera ronda.

- El personatge de l'esquerra está controlat pel **jugador**, i el de la dreta per l'**ordinador**.
- La primera ronda tindrà un **50%** de probabilitat de que començi l'ordinador.
- El **color del text** superior canviará depenent de qui está parlant: **vermell** pel jugador, **blau** per l'ordinador.

Si insulta l'ordinador, escollirà un insult aleatori, i tindrem disponibles vàries respostes per contraatacar.
Si insulta el jugador, tindrem varis insults disponibles, i l'ordinador tindrá un ***50% o un 1/16*** de probabilitats de respondre correctament.
Qui hagi guanyat la ronda serà el que insultará a la següent.

Podrem veure qui ha guanyat la ronda actual per les **animacions** (qui ataca guanya, qui és atacat perd) i per un **indicador en forma d'estrella** al costat del jugador que hagi guanyat la ronda.

Un cop un dels jugadors hagi aconseguit **3 victòries**, sortirá un missatge (segons si hem guanyat o no) i en uns *4 segons* veurem la pantalla final. Escoltarem una nova música de fons i veurem els resultats:

- **Rondes totals**
- **Victòries del jugador**
- **Victòries de l'ordinador**
- **Temps emprat**
- **Missatge** apropiat depenent de si hem guanyat o perdut.
- **Tres botons**: un per tornar al menú inicial, un per tornar a jugar, i un per sortir del joc.

A més, si premem el botó '***Esc***' durant el joc sortirá una **pantalla de pausa** on podrem tornar al menú inicial o reiniciar la partida.

## Explicació del codi

### SceneChanger.cs
Aquest script s'encarrega de controlar els **canvis d'escena**, així com l'àudio dels botons per canviar de *scene*.

### DialogueNode.cs
Aquest objecte s'encarrega d'emmagatzemar les dades pertinents al "diàleg" del joc:

- **History**: text a mostrar a la part superior.
- **Answers**: possibles respostes o insults.
- **IsFinal**: indica si quest node és final (determina qui ha guanyat la ronda)
- **NextNode**: el node que vindrá després de l'actual node.
- **OnNodeVisited**: defineix una acció que succeïrá un cop visitat aquest node.

### DialogueFiller.cs
Aquest script s'encarrega de la **lògica principal** del duel d'insults, fent servir una **màquina d'estats** per controlar l'estat del joc en tot moment. Aquest script s'encarrega de:

 - Llegir els insults i les respostes d'un **fitxer JSON** dins de la carpeta '*Resources*'.
 - Crear els nodes corresponents:
	 - **root**: El primer node, només mostra un text d'iniciació de la partida. Un cop continuem, hi ha un 50% de probabilitat de que començi la partida l'ordinador.
	 - **insultsJugador**: Aquest node conté els insults que el jugador pot utilitzar. Un cop escollim un insult, saltarem al node corresponent a aquest insult a *playerInsults*.
	 - **playerInsults**: Aquest array de nodes conté cadascun dels possibles insults que hagi utilitzat el jugador des de *insultsJugador*. Es mostra el text de l'insult escollit i una opció per continuar.
	 - **playerAnswers**: Aquest array de nodes conté cadascuna de les possibles respostes que hagi utilitzat el jugador des de les *Answers* de *computerInsults*. Es mostra el text de la resposta escollida i una opció per continuar.
	 - **computerInsults**: Aquest array de nodes conté cadascun dels possibles insults que l'ordinador utilitzará aleatoriament. Aquest node mostra al jugador les possibles respostes que pot escollir.
	 - **computerAnswers**: Aquest array de nodes conté cadascuna de les respostes possibles de l'ordinador. L'ordinador tindrá un 50% de probabilitat d'escollir la resposta correcta, així com un 50% de probabilitat d'escollir aleatòriament d'entre les possibles respostes.
	 - **playerWins**: Aquest node mostra un missatge de felicitació per guanyar. No mostra cap resposta i és final (*IsFinal*).
	 - **computerWins**: Aquest node mostra un missatge per indicar que hem perdut. No mostra cap resposta i és final (*IsFinal*).
- Control·lar el **flux del duel**, control·lant cada ronda:
	- A la primera ronda es **decidirá aleatoriament qui comença**, amb un 50% de probabilitat de que començi l'ordinador.
	- Les següents rondes les **començarà qui hagi guanyat la ronda anterior**, sempre i quan cap dels jugadors hagi obtingut 3 victòries.
	- Per cada ronda, es **mostra el diàleg** a la part superior (mostrant el color apropiat segons qui está parlant) i les **opcions disponibles** a la caixa inferior.
	- El joc control·la constantment **quantes victòries** té cada jugador. 
	- Un cop un dels jugadors consegueix obtenir **3 victòries** (siguin o no seguides) es mostrará un **missatge final** i **canviarem automàticament d'escena**. 
- Controlar les **animacions** i els **sprites**:
	- Cada personatge tindrá una **animació "*idle*"**, que es repetirá constantment.
	- Qui hagi guanyat la ronda **atacará**, i qui hagi perdut **rebrá l'atac**.
	- Qui hagi perdut el joc (qui no hagi obtingut 3 victòries) "**morirá**".
	- Per cada ronda, es marca qui ha guanyat creant una **estrella** en el costat del jugador que hagi guanyat.
- Control·lar alguns dels **sons**:
	- Quan un **jugador ataca**, escoltarem un so per indicar-ho.
	- Quan una **estrella apareix**, escoltarem un so per indicar-ho.
	- Quan un **jugador mor**, escoltarem un so per indicar-ho.

### GameManager.cs
Aquest script s'encarrega de control·lar el **funcionament principal** del joc. S'encarrega de **mostrar el text superior** amb l'animació de "*typewriter*" i un so per indicar que está escrivint, així com **instanciar cada resposta disponible** dins del *ScrollView* (la caixa inferior).

També s'encarrega de controlar les **rondes totals** i el **temps emprat** de cada partida, així com **assignar el següent *node*** segons la resposta escollida pel jugador i **canviar el color del text** superior segons qui está parlant.

Aquest script també s'encarrega de **comprovar si el jugador fa pausa** al joc prement la tecla '*Esc*'. Si es així, mostra un requadre amb un text i dos botons. Mentre el joc estigui pausat, **no es pot fer clic a cap opció** dins del quadre inferior.

Un cop es mostri un **node final**, ja que no hi haurá noves opcions, en comptes d'afegir un *Listener*, mostra un missatge de consola i **canvia a l'escena final**.

### EndGameData.cs
Aquest objecte s'encarrega d'emmagatzemar les dades informatives de l'última partida, per ser mostrades a la pantalla final. Emmagatzema: 
- **playerTotalWins**: Les victòries totals del jugador
- **computerTotalWins**: Les victòries totals de l'ordinador
- **totalRounds**: les rondes totals de la partida
- **totalTime**: el temps emprat en la partida

### EndGameScript.cs
Aquest script s'encarrega de **mostrar les dades del joc** a la pantalla final, així com un **missatge** depenent de si hem guanyat la partida o no.

## Assets utilitzats

### Sprites:
##### Player - https://assetstore.unity.com/packages/2d/characters/cute-2d-girl-wizard-155796
##### Computer - https://assetstore.unity.com/packages/2d/characters/cute-2d-college-student-198684
##### Star - https://es.m.wikipedia.org/wiki/Archivo:Star*.svg

### Backgrounds:
##### Blue - https://opengameart.org/content/free-game-gui
##### Nature - https://www.vecteezy.com/vector-art/3467248-vector-nature-landscape-background-cute-simple-cartoon-style

### Music:

##### Main Menu - https://www.youtube.com/watch?v=TuEBmXFpJ10
##### Game - https://www.youtube.com/watch?v=N6pOi8Tfhqk
##### End Screen - https://www.youtube.com/watch?v=w-_tnP0enXY

### Sounds:

##### https://opengameart.org/art-search-advanced?keys=&field_art_type_tid%5B%5D=13&sort_by=count&sort_order=DESC


### Text:

##### http://gamelosofy.com/los-insultos-de-el-secreto-de-monkey-island-1/
